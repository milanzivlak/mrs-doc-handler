from .item import Item


class Table(Item):
    item_title = "Table"
    def __init__(self, name, parent, file_path = None):
        super().__init__(name, parent)
        self.file_path = file_path 