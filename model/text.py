from .item import Item


class Text(Item):
    item_title = "Text"
    def __init__(self, name, parent, file_path = None):
        super().__init__(name, parent)
        self.file_path = file_path

    def get_path(self):
        return self.file_path  
        

    