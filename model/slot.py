from .item import Item

class Slot(Item):
    item_title = "Slot"
    child_title = "Text" # TODO : Potrebno je da sve ove klase nasljedjuju Element
    def __init__(self, name, parent):
        super().__init__(name, parent)  
        # child elementi od slota ce biti instance klase Element