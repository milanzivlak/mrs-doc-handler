from .content import Content
from .item import Item

class Document(Item):
    item_title = "Document"
    child_title = "Content"
    def __init__(self, name, parent):
        super().__init__(name, parent)        

    # elements u Document ce biti Content, Concent je child od Document-a