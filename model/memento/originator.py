from .concrete_memento import ConcreteMemento

class Originator:


    """
    Originator sadrzi neka veoma bitna stanja koja se menjaju kroz vreme.
    Takodje definise metode za cuvanje stanja u Mementu i metodu za
    vracanje stanja iz njega.
    """
    _state = None  # Mozda _state da potencijalno bude model

    def __init__(self, state = None):
        self._state = state
        #print(f"Originator: Moje pocetno stanje je: {self._state}")

    def save(self):

        return ConcreteMemento(self._state)

    def restore(self, memento = None):
        self._state = memento.get_state()
        return self._state
        #print(f"Originator: Moje stanje se promenilo u: {self._state}")