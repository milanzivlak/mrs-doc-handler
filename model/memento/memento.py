from abc import ABC, abstractmethod

class Memento(ABC):

    """
    Memento interfejs je namenjen da vrati mementove metapodatke,
    kao sto je npr. datum kreacije i ime.
    Medjutim, on ne prikazuje Originator-ovo stanje.
    """

    @abstractmethod
    def get_state(self):
        pass
    # @abstractmethod
    # def get_name(self):
    #     pass

    # @abstractmethod
    # def get_date(self):
    #     pass