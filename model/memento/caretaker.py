
class Caretaker:
    def __init__(self, originator = None):
        self._mementos = []
        self._originator = originator
        self._redos = []


    def backup(self):
        # saving Originator's state

        self._mementos.append(self._originator.save())

    def backup_redo(self):
        self._redos.append(self._originator.save())

    def undo(self):

        if len(self._mementos) == 0:
            return None

        memento = self._mementos.pop()
        self._redos.append(memento)
        #print(f"Caretaker: Vracanje stanjaa na: {memento.get_name()}")

        try:
            state = self._originator.restore(memento)
            return state

        except Exception:
            self.undo()

    def redo(self):
        if len(self._redos) == 0:
            return None

        memento = self._redos.pop()
        
        try:
            state = self._originator.restore(memento)
            return state

        except Exception:
            self.redo()
    # def show_history(self):
    #     print("Caretaker: Lista memento-a: ")
    #     for memento in self._mementos:
    #         print(memento.get_name())

