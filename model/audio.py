from .item import Item

class Audio(Item):
    item_title = "Audio"
    def __init__(self, name, parent, file_path = None):
        super().__init__(name, parent)
        self.file_path = file_path  