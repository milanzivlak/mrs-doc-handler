from abc import ABC, abstractmethod

class Element(ABC):
    
    @abstractmethod
    def serialize(self):
        pass

    @abstractmethod
    def deserialize(self):
        pass