from .document import Document
from .item import Item

class Workspace(Item):
    item_title = "Workspace"
    def __init__(self, name):
        
        super().__init__(name, "workspace")
        
    # elements u Workspace-u ce biti lista Dokumenata