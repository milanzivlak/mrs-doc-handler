from abc import ABC, abstractmethod


class CollectionHandler(ABC):


    @abstractmethod
    def insert_collection(self):
        pass

    @abstractmethod
    def insert_document(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def undo(self):
        pass

    @abstractmethod
    def redo(self):
        pass