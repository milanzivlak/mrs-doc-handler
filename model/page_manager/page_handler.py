from abc import ABC, abstractmethod


class PageHandler(ABC):


    @abstractmethod
    def insert_slot(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def undo(self):
        pass

    @abstractmethod
    def redo(self):
        pass