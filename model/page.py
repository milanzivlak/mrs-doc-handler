from .item import Item

class Page(Item):
    item_title = "Page"
    child_title = "Slot"
    def __init__(self, name, parent):
        super().__init__(name, parent)  