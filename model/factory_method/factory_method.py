from model.workspace import Workspace
from model.collection import Collection
from model.document import Document
from model.content import Content
from model.page import Page
from model.slot import Slot
from model.text import Text
from model.tree_model import TreeModel
from model.image import Image
from model.audio import Audio
from model.video import Video
from model.table import Table

class FactoryMethod:
    def __init__(self):
        pass

    def initialize(self, title, parent, filepath=None):

        # Ova metoda se koristi za kreiranje klasa u pozadini u zavisnosti od toga koja nam je potrebna
        
        if title == "Collection":
            collection = Collection("Nova kolekcija", parent)
            return collection

        if title == "Document":
            document = Document("Novi dokument", parent)
            return document

        elif title == "Content":
            content = Content("Novi content", parent)
            return content
        
        elif title == "Page":
            page = Page("Novi page", parent)
            return page

        elif title == "Slot":
            slot = Slot("Novi slot", parent)
            return slot

        elif title == "Text":
            text = Text("Novi tekst", parent, filepath)
            return text

        elif title == "Image":
            image = Image("Nova slika", parent, filepath)
            return image

        elif title == "Audio":
            audio = Audio("Novi audio", parent, filepath)
            return audio

        elif title == "Table":
            table = Table("Nova tabela", parent, filepath)
            return table

        else:
            video = Video("Novi video", parent, filepath)
            return video


