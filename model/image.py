from .item import Item

class Image(Item):
    item_title = "Image"
    def __init__(self, name, parent, file_path = None):
        super().__init__(name, parent)
        self.file_path = file_path  