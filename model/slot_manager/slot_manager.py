from PySide2 import QtWidgets, QtGui, QtCore

from .slot_controller import SlotController


class SlotManager(QtWidgets.QWidget):
    def __init__(self, parent, title):
        super().__init__(parent)
        
        self.title = title
        self.main_window = parent

        self.workspace = self.main_window.tree.workspace # root item
        self.slot_controler = SlotController()
        

        self._layout = QtWidgets.QVBoxLayout()

        self.menu_bar = QtWidgets.QMenuBar(self)
        self.edit_menu = QtWidgets.QMenu("Edit", self)
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert video',self, triggered = self.insert_video))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert audio',self, triggered = self.insert_audio))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert text',self, triggered = self.insert_text))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert image',self, triggered = self.insert_image))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert table',self, triggered = self.insert_table))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\delete-node.png"), 'Delete',self, triggered = self.delete))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-undo.png"), 'Undo',self, triggered = self.undo))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-redo.png"), 'Redo',self, triggered = self.redo))
        

        self.help_menu = QtWidgets.QMenu("Help", self)
        self.menu_bar.addMenu(self.edit_menu)
        self.menu_bar.addMenu(self.help_menu)
        self._layout.addWidget(self.menu_bar)

        

        
        self.setLayout(self._layout)

        

        for index in range(self.main_window.tab.count()):
            if self.main_window.tab.tabText(index) == self.title:
                self.main_window.tab.setCurrentIndex(index)
                return
        self.main_window.tab.addTab(self, self.title)
        self.main_window.add_widget(self.main_window.tab)
        self.main_window.central_widget.setCurrentWidget(self.main_window.tab)
        self.main_window.tab.setCurrentIndex(self.main_window.tab.count()-1)
        
        
    def insert_video(self):
        self.slot_controler.insert_video(self)
    
    def insert_audio(self):
        self.slot_controler.insert_audio(self)

    def insert_image(self):
        self.slot_controler.insert_image(self)
    
    def insert_text(self):
        self.slot_controler.insert_text(self)
        
    def insert_table(self):
        self.slot_controler.insert_table(self)

    
    def delete(self):
        self.slot_controler.delete(self)
        

    def undo(self):
        self.slot_controler.undo(self)
        

    def redo(self):
        self.slot_controler.redo(self)
        
