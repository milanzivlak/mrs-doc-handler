from PySide2 import QtWidgets, QtGui, QtCore

from .slot_handler import SlotHandler

from model.factory_method.factory_method import FactoryMethod
from model.command_pattern.command import Command
from model.command_pattern.component_command import ComponentCommand
from model.command_pattern.switch import Switch
from model.command_pattern.insert_command import InsertCommand
from model.command_pattern.delete_command import DeleteCommand
from model.command_pattern.rename_command import RenameCommand
from model.command_pattern.memento_command import MementoCommand

# Memento
from model.memento.memento import Memento
from model.memento.originator import Originator
from model.memento.caretaker import Caretaker
from model.memento.concrete_memento import ConcreteMemento

class SlotController(SlotHandler):

    def __init__(self):

        self.factory_method = FactoryMethod()

        # Memento
        self.state = []
        self.originator = Originator(self.state)
        self.caretaker = Caretaker(self.originator)
        self.caretaker.backup()

        self.command = ComponentCommand()
        self.reverse_command = MementoCommand()

        # Komande
        self.insert_command = InsertCommand(self.command)
        self.delete_command = DeleteCommand(self.command)
        self.rename_command = RenameCommand(self.command)

        # Inverzne
        self.insert_reversed = InsertCommand(self.reverse_command)
        self.delete_reversed = DeleteCommand(self.reverse_command)

        self.switch = Switch()
        self.switch.register("Insert", self.insert_command)
        self.switch.register("Delete", self.delete_command)
        self.switch.register("Insert reversed", self.insert_reversed)
        self.switch.register("Delete reversed", self.delete_reversed)
        self.switch.register("Rename", self.rename_command)


    def insert_video(self, manager):
        self.insert_element("Video", manager)
    
    def insert_audio(self, manager):
        self.insert_element("Audio", manager)

    def insert_image(self, manager):
        self.insert_element("Image", manager)
    
    def insert_text(self, manager):
        self.insert_element("Text", manager)
        
    def insert_table(self, manager):
        self.insert_element("Table", manager)

    def insert_element(self, name, manager):
        filename = QtWidgets.QFileDialog.getOpenFileName(manager,'Open File')

        if filename[0]:
            print(filename)
        index = manager.main_window.tree.selectedIndexes()
        title = name
        
        selected_name = index[0].data()

        manager.main_window.tree.lista_slotova.append(selected_name)
        for item in manager.main_window.tree.model.all_items: 
            
            if item.name == selected_name:
                child = self.factory_method.initialize(title, item.parent, filename[0])
                child.parent = item
                self.insert_command._model = manager.main_window.tree.model
                self.insert_command._child = child
                self.insert_command._index = index
                
                self.insert_command._item = item

                self.state = ["Insert", self.insert_command._model, self.insert_command._child, self.insert_command._index, self.insert_command._item]
                self.originator._state = self.state
                self.caretaker.backup()

                self.switch.execute("Insert")
                break



    def delete(self, manager):
        index = manager.main_window.tree.selectedIndexes()
        self.delete_command.model = manager.main_window.tree.model
        self.delete_command.index = index
    
        selected = index[0].data()

        for item in manager.main_window.tree.model.all_items:
            
            if item.name == selected:
                for i in item.elements:
                    del i
        element = manager.main_window.tree.model.get_item(index[0])
        self.state = ["Delete", self.delete_command.model, element, self.delete_command.index, element.parent]
        self.originator._state = self.state
        self.caretaker.backup()

        self.switch.execute("Delete")

    def undo(self, manager):
        print("Mementos: ", len(self.caretaker._mementos))
        #print(self.caretaker._mementos[1].get_state())
        memento = self.caretaker.undo()
        

        
        if memento[0] == "Insert":
            self.delete_reversed.model = manager.main_window.tree.model
            parent = memento[4]
            row = len(parent.elements)-1
            inserted_index = manager.main_window.tree.model.index(row, 0, memento[3][0])

            self.delete_reversed.index = inserted_index

            # Redo
            element = manager.main_window.tree.model.get_item(inserted_index)
            self.state = ["Delete", self.delete_reversed.model, element, self.delete_reversed.index, element.parent]
            self.originator._state = self.state
            self.caretaker.backup_redo()

            self.switch.execute("Delete reversed")

        if memento[0] == "Delete":
            self.insert_reversed.model = manager.main_window.tree.model
            self.insert_reversed._child = memento[2]
            self.insert_reversed._index = memento[3]
            
            self.insert_reversed._item = memento[4]

            # Redo
            self.state = ["Insert", self.insert_reversed._model, self.insert_reversed._child, self.insert_reversed._index, self.insert_reversed._item]
            self.originator._state = self.state
            self.caretaker.backup_redo()

            self.switch.execute("Insert")

    def redo(self, manager):
        memento = self.caretaker.redo()
        

        
        if memento[0] == "Insert":
            self.delete_reversed.model = manager.main_window.tree.model
            parent = memento[4]
            row = len(parent.elements)-1
            inserted_index = manager.main_window.tree.model.index(row, 0, memento[3][0])

            self.delete_reversed.index = inserted_index
            

            self.switch.execute("Delete reversed")

        if memento[0] == "Delete":
            self.insert_reversed.model = manager.main_window.tree.model
            self.insert_reversed._child = memento[2]
            self.insert_reversed._index = memento[3]
            
            self.insert_reversed._item = memento[4]
            self.switch.execute("Insert")