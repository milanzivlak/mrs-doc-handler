from abc import ABC, abstractmethod


class SlotHandler(ABC):


    @abstractmethod
    def insert_video(self):
        pass
    
    @abstractmethod
    def insert_audio(self):
        pass

    @abstractmethod
    def insert_image(self):
        pass
    
    @abstractmethod
    def insert_text(self):
        pass
    
    @abstractmethod
    def insert_table(self):
        pass

    @abstractmethod
    def insert_element(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def undo(self):
        pass

    @abstractmethod
    def redo(self):
        pass