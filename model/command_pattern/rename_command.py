from .i_command import ICommand

class RenameCommand(ICommand):

    def __init__(self, command, model = None, index = None):
        self._command = command
        self._model = model
        self._index = index
        self._new_name = None

    def execute(self):
        print(self._new_name)
        self._item.name = self._new_name
        self._command.rename(self._model, self._index)