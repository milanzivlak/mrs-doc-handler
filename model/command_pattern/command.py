from ..tree_model import TreeModel

# Receiver

class Command:

    def insert(self, model, index, child_title, item):
        # param parent : index[0]

        model.beginInsertRows(index[0], 0, 0)
        
        child_instance = model.create_child(child_title, item)
        success = model.insertRow(child_instance, parent = item)
        model.endInsertRows()

    def delete(self, model, index):
        model.beginRemoveRows(index[0].parent(), index[0].row(), index[0].row())
        
        success = model.removeRow(index[0].row(), parent = index[0].parent())
        print('Brisanje uspesno?:', success)
        model.endRemoveRows()


    def rename(self, model, index):
        print("Uspesna promena imena")