from .i_command import ICommand

class DeleteCommand(ICommand):

    def __init__(self, command, model = None, index = None):
        self._command = command
        self.model = model
        self.index = index

    def execute(self):
        self._command.delete(self.model, self.index)