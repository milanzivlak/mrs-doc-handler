from .i_command import ICommand

class InsertCommand(ICommand):

    def __init__(self, command, model = None, index = None, child = None, item = None):
        self._command = command
        self._model = model
        self._index = index
        self._child = child
        self._item = item

    def execute(self):
        self._command.insert(self._model, self._index, self._child, self._item)