from abc import ABC, abstractstaticmethod

class ICommand(ABC):

    @abstractstaticmethod
    def execute():
        pass