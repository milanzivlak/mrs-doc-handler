from abc import ABC, abstractmethod


class ContentHandler(ABC):


    @abstractmethod
    def insert_page(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def undo(self):
        pass

    @abstractmethod
    def redo(self):
        pass