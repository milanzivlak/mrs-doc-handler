from PySide2 import QtWidgets, QtGui, QtCore

from .document_controller import DocumentController



class DocumentManager(QtWidgets.QWidget):
    def __init__(self, parent, title):
        super().__init__(parent)
        self.title = title
        self.main_window = parent

        self.workspace = self.main_window.tree.workspace # root item
        self.document_controler = DocumentController()
        

        self._layout = QtWidgets.QVBoxLayout()

        self.menu_bar = QtWidgets.QMenuBar(self)
        self.edit_menu = QtWidgets.QMenu("Edit", self)
        #self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert collection',self, triggered = self.insert_collection))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert content',self, triggered = self.insert_content))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\delete-node.png"), 'Delete',self, triggered = self.delete))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-undo.png"), 'Undo',self, triggered = self.undo))
        self.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-redo.png"), 'Redo',self, triggered = self.redo))
        

        self.help_menu = QtWidgets.QMenu("Help", self)
        self.menu_bar.addMenu(self.edit_menu)
        self.menu_bar.addMenu(self.help_menu)
        self._layout.addWidget(self.menu_bar)


        
        self.setLayout(self._layout)

        

        for index in range(self.main_window.tab.count()):
            if self.main_window.tab.tabText(index) == self.title:
                self.main_window.tab.setCurrentIndex(index)
                return
        self.main_window.tab.addTab(self, self.title)
        self.main_window.add_widget(self.main_window.tab)
        self.main_window.central_widget.setCurrentWidget(self.main_window.tab)
        self.main_window.tab.setCurrentIndex(self.main_window.tab.count()-1)
        
        
    
    def insert_content(self):
        self.document_controler.insert_content(self)
        


    def delete(self):
        self.document_controler.delete(self)
        

    
    def undo(self):
        self.document_controler.undo(self)
        

    def redo(self):
        self.document_controler.redo(self)
        
