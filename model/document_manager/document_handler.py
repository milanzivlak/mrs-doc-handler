from abc import ABC, abstractmethod


class DocumentHandler(ABC):


    @abstractmethod
    def insert_content(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def undo(self):
        pass

    @abstractmethod
    def redo(self):
        pass