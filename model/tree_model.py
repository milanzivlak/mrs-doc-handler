from PySide2 import QtCore, QtGui, QtWidgets
#from .tree_item import TreeItem
from .document import Document
from .content import Content
from .page import Page
from .slot import Slot
from .text import Text

class TreeModel(QtCore.QAbstractItemModel):
    def __init__(self, workspace, parent=None):
        super().__init__(parent)
        self.workspace = workspace # lista dokumenata
        self.all_items = []
        self.all_items.append(self.workspace)
        self.icons = {
            "Collection": QtGui.QIcon("resources\icons\model\collection.png"),
            "Document": QtGui.QIcon("resources\icons\model\document.png"),
            "Content": QtGui.QIcon("resources\icons\model\content.png"),
            "Page": QtGui.QIcon("resources\icons\model\pages.png"),
            "Slot": QtGui.QIcon("resources\icons\model\cross.png"),
            "Text": QtGui.QIcon("resources\icons\model\icon_text.png"),
            "Image": QtGui.QIcon("resources\icons\model\image.png"),
            "Audio": QtGui.QIcon("resources\icons\model\icon_audio.png"),
            "Video": QtGui.QIcon("resources\icons\model\icon_video.png")
        }
       
    def get_item(self, index):
        # get item  umjesto get_element jer imamo klasu Element kao dio specifikacije
        
        if index.isValid():
            item = index.internalPointer()
            if item:
                return item
        return self.workspace


    def columnCount(self, parent):
        return 2

    def data(self, index, role):
        
        if not index.isValid():
            return None

        item = self.get_item(index)
        self.all_items.append(item)
        if (index.column() == 0) and (role == QtCore.Qt.DecorationRole):
            return self.icons.get(item.item_title)
        if (index.column() == 0) and (role == QtCore.Qt.DisplayRole):
            return item.name

        elif (index.column() == 1) and (role == QtCore.Qt.DisplayRole):
            return item.title
        
        

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled | QtCore.Qt.ItemIsEditable

    def headerData(self, section, orientation, role):
        # TODO: promijeniti da ne bude "Name" i "Type"
        
        if orientation != QtCore.Qt.Vertical:
            if (section == 0) and (role == QtCore.Qt.DisplayRole):
                return "Name"
            elif (section == 1) and (role == QtCore.Qt.DisplayRole):
                return "Type"

    

    def index(self, row, column, parent):
        parent_item = self.get_item(parent)
        if hasattr(parent_item, 'elements'):
            if (row >= 0) and (row < len(parent_item.elements)) and parent_item.elements[row]:
                return self.createIndex(row, column, parent_item.elements[row])
            else:
                return QtCore.QModelIndex()
        else:
            return QtCore.QModelIndex()
        

    def parent(self, child):

        if child.isValid():
            child_item = self.get_item(child)
            if hasattr(child_item, 'parent'):
                parent_item = child_item.parent
                if parent_item and (parent_item != self.workspace) and parent_item.parent:
                    return self.createIndex(parent_item.parent.elements.index(parent_item), 0, parent_item)
            else:
                return self.createIndex(child_item.index(child_item), 0, child_item)
        
        return QtCore.QModelIndex()


    def setData(self, index, value, role = QtCore.Qt.EditRole):
        """
        Unesenu vrijednost postavlja kao novu za izabrani element, onaj koji se nalazi na datom indeksu.
        """
        item = self.get_item(index)
        if value == " ":
            return False

        if role == QtCore.Qt.EditRole:
            item.name = value
            return True

        return False

    def rowCount(self, parent):
        item = self.get_item(parent)
        if hasattr(item, 'elements'):
            return len(item.elements)
        else:
            return 0

    # def removeRows(self, position, rows, index=QtCore.QModelIndex()):
    #     #parent_item = self.get_item(parent)
    #     selected_item = self.get_item(index)
    #     parent = selected_item.parent

    #     self.beginRemoveRows(parent, position, position + rows - 1)
    #     success = parent_item.removeChildren(position, rows)
    #     self.endRemoveRows()

    #     return success

    def insertRow(self, item, parent):
        # if not parent.isValid():
        #     parent = self.workspace
        # else:
        #     parent = parent.internalPointer()

        parent.appendChild(item)
    def removeRow(self, row, parent):
        if not parent.isValid():
            # parent is not valid when it is the root node, since the "parent"
            # method returns an empty QModelIndex
            parent = self.workspace
        else:
            parent = parent.internalPointer()  # the node

        parent.removeChild(row)
        return True
    def supportedDropActions(self): 
        return QtCore.Qt.CopyAction | QtCore.Qt.MoveAction  
    # def mimeTypes(self, index):
    #     item = self.get_item(index[0])
    #     return item.title
    # Dobavljanje koji element je selektovan

    # def mimeData(self, index):
    #     item = self.get_item(index)
    #     return item.elements
    

    # def dropMimeData( self, mimedata, action, row, column, parentIndex ):
    #     pass
    #     # if not mimedata.hasFormat( 'sushi-build-items' ):
    #     #     return False
    #     # data = pickle.loads((str(mimedata.data('sushi-build-items'))))
    #     # items = dataToItems(data)
    #     # self.insertItems(row, items, parentIndex)
    #     # return True


    def create_child(self, title, parent):

        # TODO: ovu metodu je potrebno prebaciti u Abstract Factory, da ona bude zaduzena za kreiranje klasa

        if title == "Document":
            document = Document("Novi dokument", parent)
            return document

        elif title == "Content":
            content = Content("Novi content", parent)
            return content
        
        elif title == "Page":
            page = Page("Novi page", parent)
            return page

        elif title == "Slot":
            slot = Slot("Novi slot", parent)
            return slot

        else:
            text = Text("Novi tekst", parent)
            return text

