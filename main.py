import sys
from PySide2 import QtWidgets, QtGui
from app import ApplicationFramework
from ui.main_window import MainWindow


if __name__ == "__main__":

    applicaton_framework = ApplicationFramework()
    applicaton_framework.login()
    sys.exit(applicaton_framework.exec_())