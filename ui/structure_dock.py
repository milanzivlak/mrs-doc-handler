from PySide2 import QtWidgets, QtGui, QtCore
from plugins.text_manager.widget import TextEdit
import os

class StructureDock(QtWidgets.QDockWidget):

    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.model = QtWidgets.QFileSystemModel()
        self.main_window = parent 
        # i ovaj ispod je zezao za ono mini polje kod menu bara
        #self.text_edit = TextEdit(self.main_window)
        #self.file_path = "data/dada.txt"
        self.file_path = None

        self.filter = [""]
        self.model.setRootPath(QtCore.QDir.currentPath())
        self.model.setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot | QtCore.QDir.AllEntries)
        # self.model.setNameFilters(self.filter)
        # self.model.setNameFilterDisables(False)
        
        self.tree = QtWidgets.QTreeView()
        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(QtCore.QDir.currentPath() + "/data"))
        self.tree.clicked.connect(self.click_action)
        
        self.setWidget(self.tree)
        self.tree.clicked.connect(self.set_root_path)

    def set_root_path(self, path):
        # self.model.setRootPath(path)
        # self.tree.setRootIndex(self.model.index(path))
        pass

    def click_action(self, index):
        self.file_path = self.model.filePath(index)
        self.extension = self.file_path.split(".")[-1]
        print(self.extension)
        #print("structure_dock.py: Kliknuta datoteka, " + self.file_path + " ")

        if self.extension == "txt":
            
            text_edit_focus = self.main_window.central_widget.childAt(0,0)
            text_edit_focus.opened_file = self.file_path
            self.main_window.text_manag(self.file_path)
            

        if self.extension == "png" or self.extension == "PNG":
            self.main_window.add_test(self.file_path)
            

    def delete_file(self):
        #self.file_path = self.model.filePath(index)

        os.remove(self.file_path)

    def create_file(self):
        self.file_path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Novi file", "file", "Text files (*.txt)")
        print(self.file_path)
        try:

            with open(self.file_path, "w") as data:
                data.write("")
                
        except IOError as ex:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska u kreiranju datoteke", "Desila se greska kod kreiranja datoteke:\n" + str(ex))
            msg.exec()
            print(ex)