from PySide2 import QtWidgets, QtGui, QtCore


class StackedWidget(QtWidgets.QStackedWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent