from PySide2 import QtWidgets, QtCore, QtGui
from PySide2.QtWidgets import QGridLayout
import json

class RenameDialog(QtWidgets.QDialog):

    def __init__(self, parent=None):
        super(RenameDialog, self).__init__(parent)
        self.setWindowTitle("Rename")
        self.resize(400, 250)
        label_name = QtWidgets.QLabel('Novo ime')
        self.new_name = QtWidgets.QLineEdit()
        self.ime = ""
        button = QtWidgets.QPushButton('Rename')
        button.clicked.connect(self.rename_check)

        layout = QGridLayout()
        layout.addWidget(label_name, 0, 0)
        layout.addWidget(self.new_name, 0, 1)
        layout.addWidget(button, 2, 0)
        
        self.setLayout(layout)

    def rename_check(self):
        msg = QtWidgets.QMessageBox()
        pravilno_ime = False
        while not pravilno_ime:
            self.ime = self.new_name.text()
            self.close()
            if self.ime:
                pravilno_ime = True
            if pravilno_ime == False:
                msg.setText("Podaci koje ste uneli nisu ispravni!")
                msg.exec_()
                break
