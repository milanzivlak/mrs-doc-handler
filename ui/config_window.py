from PySide2 import QtWidgets, QtGui, QtCore

import json

class ConfigWindow(QtWidgets.QDialog): 
    def __init__(self, parent, plugins, user):
        super().__init__(parent)
        self.plugins = plugins
        self.korisnik = user
        print(self.korisnik)    
        with open('resources/config_menu.json', 'r') as j:
            loaded = json.load(j)
            
            self.options = loaded

        # with open('resources/plugin_desc_template.html', 'r') as j:
        #     self.html_template = j.read()
        
        self.setWindowTitle("Plugins Configuration")
        self.resize(640, 480)
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(10, 440, 621, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)

        
        self.listWidget = QtWidgets.QListWidget(self)
        self.listWidget.setObjectName("listWidget")
        self.listWidget.setGeometry(QtCore.QRect(10, 10, 141, 431))

        self.add_options()
        self.listWidget.clicked.connect(self.get_option_items)
        # self.listWidget.addItem(QtWidgets.QListWidgetItem("test1"))
        # self.listWidget.addItem(QtWidgets.QListWidgetItem("test2"))

        self.listWidget_2 = QtWidgets.QListWidget(self)
        self.listWidget_2.setObjectName("listWidget_2")
        self.listWidget_2.setGeometry(QtCore.QRect(170, 10, 141, 431))
        self.listWidget_2.clicked.connect(self.get_item_desc)
        
        self.textBrowser = QtWidgets.QTextBrowser(self)
        self.textBrowser.setObjectName("textBrowser")
        self.textBrowser.setGeometry(QtCore.QRect(330, 10, 301, 421))

        
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        QtCore.QMetaObject.connectSlotsByName(self)
        self.show()


    def add_options(self):
        for item in self.options["options"]:
            if self.korisnik != "admin" and item == "Activated" or item == "Deactivated":
                self.listWidget.addItem(QtWidgets.QListWidgetItem(item))
            if self.korisnik == "admin":
                self.listWidget.addItem(QtWidgets.QListWidgetItem(item))
        return 

    def get_option_items(self, selected_option):
        self.listWidget_2.clear()
        selected = selected_option.data()
        show = []

        if(selected == "All"):
            show = self.plugins
        elif (selected == "Installed"):
            show = [plugin for plugin in self.plugins if plugin.installed]
        elif (selected == "Deinstalled"):
            show = [plugin for plugin in self.plugins if not plugin.installed]
        elif (selected == "Activated"):
            show = [plugin for plugin in self.plugins if plugin.activated]
        elif (selected == "Deactivated"):
            show = [plugin for plugin in self.plugins if not plugin.activated]

        
        for item in show:
            item_to_add = QtWidgets.QListWidgetItem()
            item_to_add.setText(item.plugin_specification.name)
            item_to_add.setData(QtCore.Qt.UserRole, item)
            self.listWidget_2.addItem(QtWidgets.QListWidgetItem(item_to_add))
        return 

    def get_item_desc(self, selected_item):
        with open('resources/plugin_desc_template.html', 'r') as j:
            self.html_template = j.read()

        item = selected_item.data(QtCore.Qt.UserRole).plugin_specification
        
        self.html_template = self.html_template.replace("{{HEADER}}", item.name)
        self.html_template = self.html_template.replace("{{VERSION}}","Version: " + item.version)
        self.html_template = self.html_template.replace("{{CORE_VERSION}}","Core version: " + item.core_version)
        self.html_template = self.html_template.replace("{{CATEGORY}}","Category: " + item.category)
        self.html_template = self.html_template.replace("{{LICENSE}}","License: " + item.licence)
        self.html_template = self.html_template.replace("{{DESCHEADER}}", "Description:")
        self.html_template = self.html_template.replace("{{DESCRIPTION}}", item.description)
        self.html_template = self.html_template.replace("{{AUTHHEADER}}", "Authors")
        authors = ""
        for author in item.authors:
            authors += author.first_name + " " + author.last_name + " " + author.email + " " + author.web_page + "<br>"
        self.html_template = self.html_template.replace("{{AUTHORS}}", authors)
        self.textBrowser.setText(self.html_template)
    

    
