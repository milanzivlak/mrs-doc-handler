from PySide2 import QtWidgets, QtGui, QtCore

class StatusBar(QtWidgets.QStatusBar):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent