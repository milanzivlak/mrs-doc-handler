from PySide2 import QtWidgets, QtGui, QtCore

import json

class HelpWindow(QtWidgets.QDialog): 
    def __init__(self, parent):
        super().__init__(parent)
        self.load_help()
        self.setWindowTitle("Help")
        self.resize(640, 480)
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(10, 440, 621, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)

        
        self.listWidget = QtWidgets.QListWidget(self)
        self.listWidget.setObjectName("listWidget")
        self.listWidget.setGeometry(QtCore.QRect(10, 10, 141, 431))

        self.add_options()
        self.listWidget.clicked.connect(self.get_item_desc)

        self.textBrowser = QtWidgets.QTextBrowser(self)
        self.textBrowser.setObjectName("textBrowser")
        self.textBrowser.setGeometry(QtCore.QRect(161, 10, 460, 431))

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        QtCore.QMetaObject.connectSlotsByName(self)
        self.show()

    def load_help(self):
        with open('resources/help/help_menu.json', 'r') as j:
            loaded = json.load(j)
            
            self.options = loaded["help"]

        
        
    def add_options(self):
        for item in self.options:
            item_to_add = QtWidgets.QListWidgetItem()
            item_to_add.setText(item["name"])
            item_to_add.setData(QtCore.Qt.UserRole, item["path"])
            self.listWidget.addItem(QtWidgets.QListWidgetItem(item_to_add))
        
    def get_item_desc(self, selected_item):
        item = selected_item.data(QtCore.Qt.UserRole)
        with open(item, 'r') as j:
            self.html_template = j.read()
        self.textBrowser.setText(self.html_template)
    

    
