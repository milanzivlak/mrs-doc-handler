from PySide2 import QtWidgets, QtCore, QtGui
from PySide2.QtWidgets import QGridLayout
import json
from resources.users.users import User
#from .main_window import MainWindow

class Login(QtWidgets.QDialog):

    def __init__(self, parent=None):
        super(Login, self).__init__(parent)
        self.setWindowTitle("Login")
        self.resize(400, 250)
        self.ulogovan = False
        self.ulogovan_korisnik = ""
        label_name = QtWidgets.QLabel('Korisnicko ime')
        self.username = QtWidgets.QLineEdit()
        

        label_password = QtWidgets.QLabel('Lozinka')
        self.lozinka = QtWidgets.QLineEdit()

        button = QtWidgets.QPushButton('Login')
        button.clicked.connect(self.login_check)

        layout = QGridLayout()
        layout.addWidget(label_name, 0, 0)
        layout.addWidget(self.username, 0, 1)
        layout.addWidget(label_password, 1, 0)
        layout.addWidget(self.lozinka, 1, 1)
        layout.addWidget(button, 2, 0)
        
        self.setLayout(layout)
        
        # self.button.clicked.connect(self.greetings)

        with open('resources/users/korisnici.json', 'r') as j:
            loaded = json.load(j)
            #print(loaded["korisnici"])
            self.korisnici = loaded
            print(self.korisnici)

    def login_check(self):

        msg = QtWidgets.QMessageBox()
        ulogovan = False
        while not ulogovan:
            for i in self.korisnici:
                if self.username.text() == i["ime"] and self.lozinka.text() == i["sifra"]:
                    msg.setText("Uspesno ste se ulogovali!")
                    msg.exec()
                    self.ulogovan = True
                    self.ulogovan_korisnik = self.username.text()
                    self.user = User(i["ime"], i["sifra"], i["plugini"])
                    print(self.user.get_ime())
                    print(self.user.get_plugini())
                    self.close()
                    ulogovan = True
            if ulogovan == False:
                msg.setText("Podaci koje ste uneli nisu pronadjeni!")
                msg.exec_()
                break
