from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import QGraphicsScene, QGraphicsView, QGraphicsItem
from PySide2.QtGui import QPen, QBrush, QPainter
from PySide2.QtCore import Qt, QPoint

class ClickableLabel(QtWidgets.QLabel):
    """
        A Label that emits a signal when clicked.
    """
    old_pos = QPoint()
    clicked = QtCore.Signal()

    def __init__(self, *args):
        super().__init__(*args)

    #def mousePressEvent(self, event):
        #self.clicked.emit()
        
    def mousePressEvent(self, evt):
        """Select the toolbar."""
        if evt.type() == QtCore.QEvent.MouseButtonPress:
            if evt.button() == QtCore.Qt.RightButton:
                self.old_pos = evt.globalPos()
            else:
                self.clicked.emit()

    def mouseMoveEvent(self, evt):
        
        """Move the toolbar with mouse iteration."""

        delta = QPoint(evt.globalPos() - self.old_pos)
        self.move(self.x() + delta.x(), self.y() + delta.y())
        self.old_pos = evt.globalPos()

class GraphicsElement(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent
        self.lista_putanja = []
        self.lista_pagea =[]
        self.pomocna_lista =[]
        #print(self.lista_putanja)
        
        #self._layout = QtWidgets.QVBoxLayout()
        # self.scene = QGraphicsScene(self)
        
        # self.graphic = QGraphicsView(self.scene, self)
        # for index in range(self.main_window.tab.count()):
        #     if self.main_window.tab.tabText(index) == "Graphics element":
        #         self.main_window.tab.setCurrentIndex(index)
        #         return
        # self.main_window.tab.addTab(self.graphic, "Graphics element")
        # self.main_window.central_widget.setCurrentWidget(self.main_window.tab)
        # self.main_window.tab.setCurrentIndex(self.main_window.tab.count()-1)
        
    def page(self):
        self.scene = QGraphicsScene(self)
        
        self.graphic = QGraphicsView(self.scene, self)
        for index in range(self.main_window.tab.count()):
            if self.main_window.tab.tabText(index) == "Graphics element":
                self.main_window.tab.setCurrentIndex(index)
                return
        self.main_window.tab.addTab(self.graphic, "Graphics element")
        self.main_window.central_widget.setCurrentWidget(self.main_window.tab)
        self.main_window.tab.setCurrentIndex(self.main_window.tab.count()-1)
        
        counter_y = 20
        for i in self.lista_pagea:
            self.create_slot(i, counter_y)
           
            counter_y += 120
            
    def create_slot(self, i, counter_y):
        
        #counter_y = 20
        widg = ClickableLabel()
        widg.setText(i.name)
        widg.setGeometry(60, counter_y, 300, 100)
        self.scene.addWidget(widg)
        
        lista = []
        for a in i.elements:
            
            lista.append(a.file_path)
            
        widg.clicked.connect(lambda lista = lista: self.info(lista))

        
    def info(self, lista):
        self.scene2 = QGraphicsScene(self)
        self.graphics2 = QGraphicsView(self.scene2, self)
        
        self.main_window.tab.addTab(self.graphics2, "Graphics element")
        self.main_window.central_widget.setCurrentWidget(self.main_window.tab)
        self.main_window.tab.setCurrentIndex(self.main_window.tab.count()-1)

        for i in lista:
            path = i
            
            extension = path.split(".")[-1]
            
            print(extension)
            
            if extension == "PNG" or extension == "jpg" or extension == "png":
                self.create_image(path, extension)
                
            if extension == "txt":
                self.create_txt(path, extension)
                
            if extension == "mp3":
                self.create_audio(path, extension)

            if extension == "mp4":
                self.create_video(path, extension)
            
            if extension == "json" or extension == "pkl":
                self.create_table(path, extension)

    def create_image(self, path, extension):
        self.path_img = ""
        self.ext_img = ""
        thispixmap = QtGui.QPixmap(path)
        widg = ClickableLabel()
        widg.setPixmap(thispixmap.scaled(110, 100))
        self.scene2.addWidget(widg)
        self.path_img = path
        self.ext_img = extension
        widg.clicked.connect(lambda path=path, extension = extension: self.load_plugin(path, extension))
    

    def create_txt(self, path, extension):
        self.path_txt = ""
        self.ext_txt = ""
        #self.path = i
        widg1 = ClickableLabel()
        widg1.setGeometry(90,155, 100, 100)
        with open(path, "r") as d:
            tekst = d.read()
        widg1.setText(tekst)
        self.scene2.addWidget(widg1)
        self.path_txt = path
        self.ext_txt = extension
        widg1.clicked.connect(lambda path=path, extension = extension: self.load_plugin(path, extension))
    
    def create_audio(self, path, extension):
        self.path_audio = ""
        self.ext_audio = ""
        widg1 = ClickableLabel()
        widg1.setGeometry(150,155, 100, 100)
        widg1.setText("audio_sample.mp3")
        self.scene2.addWidget(widg1)
        self.path_audio = path
        self.ext_audio = extension
        widg1.clicked.connect(lambda path=path, extension = extension: self.load_plugin(path, extension))

    def create_video(self, path, extension):
        self.path_audio = ""
        self.ext_audio = ""
        widg1 = ClickableLabel()
        widg1.setGeometry(150,200, 100, 100)
        widg1.setText("720.mp4")
        self.scene2.addWidget(widg1)
        self.path_audio = path
        self.ext_audio = extension
        widg1.clicked.connect(lambda path=path, extension = extension: self.load_plugin(path, extension))
    
    def create_table(self, path, extension):
        self.path_audio = ""
        self.ext_audio = ""
        widg1 = ClickableLabel()
        widg1.setGeometry(100, 100, 100, 100)
        widg1.setText("Tabela")
        self.scene2.addWidget(widg1)
        self.path_audio = path
        self.ext_audio = extension
        widg1.clicked.connect(lambda path=path, extension = extension: self.load_plugin(path, extension))

    def load_plugin(self, path, extension):
        self.main_window.check_extension_2(path,extension)



        