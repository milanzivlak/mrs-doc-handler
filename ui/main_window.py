from PySide2 import QtWidgets, QtGui, QtCore
from .menu_bar import MenuBar
from .status_bar import StatusBar
from .structure_dock import StructureDock
from .tool_bar import ToolBar
from .stacked_widget import StackedWidget
from .structure_dock import StructureDock
from .config_window import ConfigWindow
from .help_window import HelpWindow
from plugins.text_manager.widget import TextEdit
from plugins.image_manager.widget import ImageEdit
from PySide2.QtWidgets import QVBoxLayout, QTextEdit
from model.workspace_manager.workspace_manager import WorkspaceManager
from .tree import Tree
from resources.users.users import User

import sys
import json


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, title, icon, parent=None):
        super().__init__(parent)
        self.setWindowTitle(title)
        self.setWindowIcon(icon)
        self.resize(800, 600)
       
        

        self.permisije = {}
        #self.layout = QtWidgets.QVBoxLayout()

        self.tab = QtWidgets.QTabWidget()
        self.tab.setTabsClosable(True)
        self.tab.tabCloseRequested.connect(self.delete_tab)
        self.edit_menu = QtWidgets.QMenu("&Edit")
        self.edit_menu.addAction(QtWidgets.QAction("Workspace Manager", self, triggered = self.add_workspace_tab))
        self.plugin_registry = None
        #meni
        self.menu_bar = MenuBar(self)
        #toolbar
        self.tool_bar = ToolBar(self)
        #self.logout_button.clicked.connect(self.close)
        #self.logout_button.setGeometry(200, 200, 200, 200)
        #statusbar
        self.status_bar = StatusBar(self)
        #self.status_bar.addWidget(self.logout_button)
        #centralwidget
        self.central_widget = StackedWidget(self)
        # ovaj ispod je zezao za ono mini polje
        #self.text_edit = TextEdit(self.central_widget)
        # self.test = Test(self.central_widget)
        self.widg = QtWidgets.QLabel("Plugin trenutno nije aktivan!")
        self.widg.setAlignment(QtCore.Qt.AlignCenter)
        self.widg.setFont(QtGui.QFont('Arial', 20))
        # self.structure_dock = StructureDock("File browser", self)
        # self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.structure_dock)
        self.dock = QtWidgets.QDockWidget("Document browser", self)
        self.tree = Tree(self.dock)
        dock_layout = QVBoxLayout()
        

        self.search_form = QtWidgets.QLineEdit()
        self.search_form.setMaximumWidth(150)
        self.search_form.setMaximumHeight(25)
        self.search_form.setPlaceholderText("Unesite za pretragu")
        self.search_dugme = QtWidgets.QPushButton("Pretrazi")

        dock_layout.addWidget(self.search_form)
        dock_layout.addWidget(self.search_dugme)
        dock_layout.addWidget(self.tree)

        multi_widget = QtWidgets.QWidget()
        multi_widget.setLayout(dock_layout)
        
        self.dock.setWidget(multi_widget)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.dock)

        #self.search_form.textChanged.connect(self.search_value)
        self.search_dugme.clicked.connect(self.search_value)


        # Tab widget


        #FIXME: prebaciti funkcije koje se trigeruju u Tree
        # self.tool_bar.addAction(QtGui.QIcon("resources/icons/new_file.png"), "Create file", self.structure_dock.create_file)
        # self.tool_bar.addAction(QtGui.QIcon("resources/icons/delete_file.png"), "Delete file", self.structure_dock.delete_file)
        

        self.document = None # otvoreni dokument (trenutno aktivni dokument)

        self.actions_dict = {
            # FIXME: ispraviti ikonicu na X
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/cross.png"), "&Quit", self),
            "save": QtWidgets.QAction(QtGui.QIcon("resources/icons/save.png"), "&Save", self),
            "help": QtWidgets.QAction(QtGui.QIcon(""), "&Help", self),
            "config": QtWidgets.QAction(QtGui.QIcon(""), "&Configuration", self),
            "pristup": QtWidgets.QAction(QtGui.QIcon("resources/icons/lock.png"), "&Pristup", self),
            "dodaj korisnika": QtWidgets.QAction(QtGui.QIcon("resources/icons/lock.png"), "&Dodaj korisnika", self)
            # TODO: dodati i ostale akcije za help i za npr. osnovno za dokument
            # dodati open...
        }

        self._bind_actions()

        self._populate_menu_bar()

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        #self.read_from_file(self.structure_dock.file_path)
        self.setCentralWidget(self.central_widget)
        self.ulogovan_korisnik = []

    def search_value(self):
        tekst = self.search_form.text()
        self.tree.search(tekst)

    def add_workspace_tab(self):
        self.workspace_manager = WorkspaceManager(self, "Workspace Manager")

    def delete_tab(self, index):
        self.tab.removeTab(index)
        if self.tab.count() == 0:
            self.remove_widget(self.tab)

    # Funkcija koja adminu dodaje menu sa pristupom
    def korisnik_admin(self, korisnik):
        print("ovaj",korisnik)
        if korisnik == "admin":
            pristup_menu = QtWidgets.QMenu("&Admin panel")
            pristup_menu.addAction(self.actions_dict["pristup"])
            pristup_menu.addAction(self.actions_dict["dodaj korisnika"])
            self.menu_bar.addMenu(pristup_menu)
        
        self.ulogovan_korisnik.append(korisnik)
        print(self.ulogovan_korisnik) 

    def _populate_menu_bar(self):
        file_menu = QtWidgets.QMenu("&File")
        help_menu = QtWidgets.QMenu("&Help")
        

        file_menu.addAction(self.actions_dict["quit"])
        file_menu.addAction(self.actions_dict["save"])
        help_menu.addAction(self.actions_dict["config"])
        help_menu.addAction(self.actions_dict["help"])

        self.menu_bar.addMenu(file_menu)
        self.menu_bar.addMenu(self.edit_menu)
        self.menu_bar.addMenu(help_menu)

    def _bind_actions(self):
        self.actions_dict["quit"].setShortcut("Ctrl+Q")
        self.actions_dict["quit"].triggered.connect(self.close)
        self.actions_dict["save"].setShortcut("Ctrl+Shift+S")
        self.actions_dict["save"].triggered.connect(self.save)
        self.actions_dict["config"].triggered.connect(self.open_configuration)
        self.actions_dict["help"].triggered.connect(self.open_help)
        self.actions_dict["pristup"].triggered.connect(self.allow_plugin)
        self.actions_dict["dodaj korisnika"].triggered.connect(self.add_user_form)


    def add_widget(self, widget):
        """
        Adds widget to central (stack) widget
        """
        self.central_widget.addWidget(widget)
        #self.central_widget.insertWidget(0, widget)

    def remove_widget(self, widget):
        self.central_widget.removeWidget(widget)

    # Funkcija koja podize widget za pristup i u njega smesta sve labele i buttone za potvrdu/odbijanje
    def allow_plugin(self):
        with open('resources/users/permissions.json', 'r') as j:
            loaded = json.load(j)
            
            self.plugini = loaded
            #print(self.plugini)
        
        self.dialog = QtWidgets.QDialog()
        layout = QVBoxLayout()

        lista = []
        self.ime = ""
        self.plugin = ""
        self.ime1 = ""
        self.plugin1 = ""
        self.ime2 = ""
        self.plugin2 = ""
        current = 0
        for i in self.plugini:
            for key in i:
                tekst = "Korisnik {} zahteva pristup pluginu {}".format(key, i[key])
                print(tekst)
                lista.append(tekst)
                if current == 0:
                    self.ime = key
                    self.plugin = i[key]
                    current +=1
                elif current == 1:
                    self.ime1 = key
                    self.plugin1 = i[key]
                    current +=1
                elif current == 2:
                    self.ime2 = key
                    self.plugin2 = i[key]
                #print("aaa", lista)
        self.tekst = ""
        for i in range(len(lista)):
            if i == 0:
                self.tekst = lista[i]
                self.label = QtWidgets.QLabel(self.tekst)
                #self.label.setAlignment(QtCore.Qt.AlignCenter)
                self.label.setFont(QtGui.QFont('Arial', 10))
                self.dugme = QtWidgets.QPushButton("Odobri", self.label)
                self.dugme_odbij = QtWidgets.QPushButton("Odbij", self.label)
                if len(lista) >= 3:
                    self.dugme.setGeometry(60, 85, 75, 25)
                    self.dugme_odbij.setGeometry(150, 85, 75, 25)
                if len(lista) == 2:
                    self.dugme.setGeometry(60, 115, 75, 25)
                    self.dugme_odbij.setGeometry(150, 115, 75, 25)
                if len(lista) == 1:
                    self.dugme.setGeometry(60, 210, 75, 25)
                    self.dugme_odbij.setGeometry(150, 210, 75, 25)
                layout.addWidget(self.label)
                self.dugme.clicked.connect(lambda : self.button_click(self.ime, self.plugin))
                self.dugme_odbij.clicked.connect(lambda : self.button_reject(self.ime, self.plugin))
            if i == 1:
                self.tekst = lista[i]
                self.label1 = QtWidgets.QLabel(self.tekst)
                #self.label1.setAlignment(QtCore.Qt.AlignCenter)
                
                self.label1.setFont(QtGui.QFont('Arial', 10))
                self.dugme1 = QtWidgets.QPushButton("Odobri", self.label1)
                self.dugme_odbij1 = QtWidgets.QPushButton("Odbij", self.label1)
                if len(lista) == 2:
                    self.dugme1.setGeometry(60, 115, 75, 25)
                    self.dugme_odbij1.setGeometry(150, 115, 75, 25)
                if len(lista) >= 3:
                    self.dugme1.setGeometry(60, 85, 75, 25)
                    self.dugme_odbij1.setGeometry(150, 85, 75, 25)
                layout.addWidget(self.label1)
                self.dugme1.clicked.connect(lambda : self.button_click(self.ime1, self.plugin1))
                self.dugme_odbij1.clicked.connect(lambda : self.button_reject(self.ime1, self.plugin1))
            if i == 2:
                self.tekst = lista[i]
                self.label2 = QtWidgets.QLabel(self.tekst)
                self.label2.setAlignment(QtCore.Qt.AlignCenter)
                self.label2.setFont(QtGui.QFont('Arial', 10))
                self.dugme2 = QtWidgets.QPushButton("Odobri", self.label2)
                self.dugme_odbij2 = QtWidgets.QPushButton("Odbij", self.label2)
                self.dugme2.setGeometry(60, 85, 75, 25)
                self.dugme_odbij2.setGeometry(150, 85, 75, 25)
                layout.addWidget(self.label2)
                self.dugme2.clicked.connect(lambda : self.button_click(self.ime2, self.plugin2))
                self.dugme_odbij2.clicked.connect(lambda : self.button_reject(self.ime2, self.plugin2))
        
        self.dialog.setWindowTitle("Admin panel")
        self.dialog.resize(300, 400)
        
        self.dialog.setLayout(layout)
        self.dialog.show()
    
    # Funkcija koja se poziva nakon sto se klikne na dugme odbij u pristup widgetu,
    # brise dict iz permisson.json
    def button_reject(self, ime, plugin):
        with open('resources/users/permissions.json', 'r') as j:
            ucitani = json.load(j)
            #print(permisije)

        counter = 0
        for i in ucitani:
            for key in i:
                if key == ime and i[key] == plugin:
                    del ucitani[counter]
                    with open('resources/users/permissions.json', 'w') as w:
                        json.dump(ucitani, w)
                    self.dialog.close()
                    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Odbijanje plugina", "Dodavanje plugina odbijeno!!")
                    msg.exec_()
                    self.allow_plugin()
                else:
                    counter +=1


    # Funkcija koja se poziva nakon sto se u pristup widgetu klikne na button odobri,
    # prosledjuje se ime korisnika koji zahteva plugin i koji plugin je u pitanju
    # a zatim dodaje u korisnici.json korisniku u listu plugina taj plugin i brise iz permissions.json dict
    def button_click(self, ime, plugin):
        #print(ime, plugin, "ok")

        with open('resources/users/korisnici.json', 'r') as j:
            loaded = json.load(j)
            self.korisnici = loaded

            #print(self.korisnici)

        counter = 0
        for i in self.korisnici:
            if i["ime"] == ime:
                #print(i)
                i['plugini'].append(plugin)
                #print(i)
                #print(counter)
                self.korisnici[counter] = i
                #print(self.korisnici)
                with open('resources/users/korisnici.json', 'w') as j:
                    json.dump(self.korisnici, j, indent=4)
                j.close()
               
            else:
                counter += 1
        self.delete_permission(ime, plugin)

    # Funkcija koja iz permission.json sklanja odabrani dict i refreshuje dialog
    def delete_permission(self, ime, plugin):
        
        with open('resources/users/permissions.json', 'r') as b:
            ucitani = json.load(b)
            print(ucitani)

        counter = 0
        for i in ucitani:
            for key in i:
                if key == ime and i[key] == plugin:
                    del ucitani[counter]
                    with open('resources/users/permissions.json', 'w') as w:
                        json.dump(ucitani, w)
                    self.dialog.close()
                    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Dodavanje plugina", "Plugin je uspesno dodat!")
                    msg.exec_()
                    self.allow_plugin()
                else:
                    counter +=1

    def save(self):
        self.tree.serialize()

    def open_configuration(self):
        user = ""
        for i in self.ulogovan_korisnik:
            user = i
        
        self.config = ConfigWindow(self, self.plugin_registry._plugins, user)


    def open_help(self):
        self.help = HelpWindow(self)
    
    def check_extension_2(self, path, extension):


        for plugin in self.plugin_registry._plugins:
            test = True
            if plugin.activated == True and extension in plugin.plugin_specification.extensions:
                # TODO: potrebno je da svaki plugin ima istu funkciju preko koje dobija path i dalje koristi
                # plugin.in_focus(path) mozda
                plugin.in_focus(path)
                test = False
                break
            
        if test == True:
            # self.add_widget(self.widg)
            # self.central_widget.setCurrentWidget(self.widg)
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Greska", "Plugin nije aktiviran!")
            msg.exec_()
            # break
            print("Plugin for certain extension is not activated!")
    
    # Funkcija koja dodaje u permission.json dict sa imenom korisnika koji zahteva i plugin koji zahteva
    # ukoliko vec postoji taj dict, dobija msg da je na cekanju
    def add_plugin(self, plugin):

        with open('resources/users/permissions.json', 'r') as j:
            loaded = json.load(j)
        
        for i in self.ulogovan_korisnik:
            korisnik = i
        
        if {korisnik : plugin} not in loaded:
            self.plugini = loaded
            self.plugini.append({korisnik : plugin})

            with open('resources/users/permissions.json', 'w') as f:
                json.dump(self.plugini, f)
        else:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Abort", "Vas zahtev za rad sa ovim pluginom je vec zabelezen!")
            msg.exec_()

    def add_user_form(self):
        self.dialog = QtWidgets.QDialog()
        self.button = QtWidgets.QPushButton("Dodaj")
        layout = QtWidgets.QVBoxLayout()

        self.ime_label = QtWidgets.QLabel("Ime:")
        self.ime_value = QtWidgets.QLineEdit()

        self.sifra_label = QtWidgets.QLabel("Sifra:")
        self.sifra_value = QtWidgets.QLineEdit()

        layout.addWidget(self.ime_label)
        layout.addWidget(self.ime_value)
        layout.addWidget(self.sifra_label)
        layout.addWidget(self.sifra_value)
        layout.addWidget(self.button)
        self.dialog.setWindowTitle("Dodavanje korisnika")
        self.button.clicked.connect(self.add_user)
        self.dialog.setLayout(layout)

        self.dialog.exec_()

    def add_user(self):
        plugin = []
        with open('resources/users/korisnici.json', 'r') as j:
            loaded = json.load(j)
            self.korisnici = loaded
        
        msg = QtWidgets.QMessageBox()
        for i in self.korisnici:
            if self.ime_value.text() == i["ime"]:
                print("isto ime")
                msg.setText("Ime koje ste uneli vec postoji!")
                msg.exec_()
                break
                
            else:
                ime = self.ime_value.text()
                sifra = self.sifra_value.text()

                dict = {"ime" : ime, "sifra": sifra, "plugini" : []}
                self.korisnici.append(dict)
                
                with open('resources/users/korisnici.json', 'w') as j:
                    json.dump(self.korisnici, j, indent = 4)

                msg.setText("Uspesna registracija!")
                msg.exec_()
                self.dialog.close()
                break

        

    # def text_manag(self, path):
    #     for pl in self.plugin_registry._plugins:
    #         if pl.plugin_specification.name != "Text Manager" and pl.plugin_specification.name != "Structure widget":
    #             self.remove_widget(pl.widget)


    #     for plugin in self.plugin_registry._plugins:
    #         if plugin.plugin_specification.name == "Text Manager" and plugin.activated == True:
    #             self.remove_widget(self.widg)
                
    #             plugin.widget.open_file(path)
    #             self.add_widget(plugin.widget)
    #         else:
    #             self.add_widget(self.widg)
                
    #             #print("Opened file path: {}".format(self.text_edit.opened_file))
    #             #self.setCentralWidget(self.central_widget)

    # def add_test(self, path):
    #     for pl in self.plugin_registry._plugins:
    #         if pl.plugin_specification.name != "Test plugin" and pl.plugin_specification.name != "Structure widget":
    #             self.remove_widget(pl.widget)


    #     for plugin in self.plugin_registry._plugins:
    #         if plugin.plugin_specification.name == "Test plugin" and plugin.activated == True:
    #             self.remove_widget(self.widg)
    #             # dobavlja sliku preko prosledjene putanje iz clickaction(sdock) i postavlja u qpixmap sliku
    #             # preko plugina dolazi do widgeta od image edita (testplugin) i setuje mu za sliku thispixmap
    #             thispixmap = QtGui.QPixmap(path)
    #             plugin.widget.image_edit.setPixmap(thispixmap)
    #             self.add_widget(plugin.widget)
                
    #             #self.setCentralWidget(self.central_widget)
    #         else:
    #             #TODO: nakon sto je uradjen login, ovo se ne pojavljuje korisnicima koji nemaju taj plugin, sto je kul,
    #             # ali treba namestiti da imaju drugi ispis, tj da nemaju pristup tom pluginu
    #             self.add_widget(self.widg) 
                
                
    # TODO: dodati metodu koja ce u fokus staviti trenutko aktivni plugin (njegov widget)

    def progres(self, aktivacija):
        if aktivacija == True:
            self.active = True
            self.progressbar = QtWidgets.QProgressDialog("Aktivacija plugina u toku..", "Cancel", 0, 100)
            self.progressbar.setWindowTitle("Plugin Manager")
        else:
            self.active = False
            self.progressbar = QtWidgets.QProgressDialog("Deaktivacija plugina u toku..", "Cancel", 0, 100)
            self.progressbar.setWindowTitle("Plugin Manager")
        self.progressbar.setMinimum(0)
        self.progressbar.setMaximum(100)
        #self.progressbar.show()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.handleTimer)
        self.timer.start(20)
        self.progressbar.exec_()
        
    def handleTimer(self):
        value = self.progressbar.value()
        if value < 100:
            value = value + 1
            self.progressbar.setValue(value)
        if value == 100:
            self.timer.stop()
            if self.active == True:
                msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Aktiviranje plugina", "Plugin je uspesno aktiviran!")
            else:
                msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Deaktiviranje plugina", "Plugin je uspesno deaktiviran!")
            msg.exec()
