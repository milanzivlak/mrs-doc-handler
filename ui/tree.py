import pickle

from PySide2 import QtWidgets, QtGui, QtCore
from model.workspace import Workspace
from model.collection import Collection
from model.document import Document
from model.content import Content
from model.page import Page
from model.slot import Slot
from model.text import Text
from model.tree_model import TreeModel
from model.image import Image
from model.audio import Audio
from model.video import Video
from ui.rename_dialog import RenameDialog
# Command Pattern
from model.command_pattern.command import Command
from model.command_pattern.switch import Switch
from model.command_pattern.insert_command import InsertCommand
from model.command_pattern.delete_command import DeleteCommand
from model.command_pattern.rename_command import RenameCommand

from model.collection_manager.collection_manager import CollectionManager
from model.document_manager.document_manager import DocumentManager
from model.content_manager.content_manager import ContentManager
from model.page_manager.page_manager import PageManager
from model.slot_manager.slot_manager import SlotManager

from .graphics_view import GraphicsElement
import threading, time

class Tree(QtWidgets.QTreeView):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent.parentWidget()
        self.postoji_content = False
        # lista ispod predstavlja listu svih elemenata u selektovanom slotu
        self.lista = []
        # lista ispod predstavlja listu svih elemenata u selektovanom pageu
        self.lista_page = []
        self.already_exists = False

        # Receiver
        self.command = Command()

        # Komande
        self.insert_command = InsertCommand(self.command)
        self.delete_command = DeleteCommand(self.command)
        self.rename_command = RenameCommand(self.command)

        self.switch = Switch()
        self.switch.register("Insert", self.insert_command )
        self.switch.register("Delete", self.delete_command)
        self.switch.register("Rename", self.rename_command)


        f = QtCore.QFile('ui\default.txt')
        f.open(QtCore.QIODevice.ReadOnly)
        f.close()


        # Context menu
        self.menu = QtWidgets.QMenu('Menu', self)
        #self.menu.addAction(QtWidgets.QAction('Test',self, triggered = self.print_name))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\document-rename.png"), 'Rename',self, triggered = self.rename))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\delete-node.png"), 'Delete',self, triggered = self.delete))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert',self, triggered = self.insert))

        # TODO: implementirati Undo i Redo
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-undo.png"), 'Undo',self))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-redo.png"), 'Redo',self))
        # Iste akcije za Edit u MenuBar-u
        # self.main_window.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\document-rename.png"), 'Rename',self, triggered = self.rename))
        # self.main_window.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\delete-node.png"), 'Delete',self, triggered = self.delete))
        # self.main_window.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\insert-child-node.png"), 'Insert',self, triggered = self.insert))
        # self.main_window.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-undo.png"), 'Undo',self))
        # self.main_window.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\icon-redo.png"), 'Redo',self))
        # self.main_window.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\desktop-image.png"), "Show", self, triggered = self.show))
        # TODO: Kreiranje Workspace-a, Documents-a itd prebaciti u metodu populate_data npr
        self.workspace = Workspace("Workspace")
        #self.populate_data()
        self.file_path = None
       
        
        self.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.clicked.connect(self.item_clicked)

        # Drag and drop enabled
        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        #self.customContextMenuRequested.connect(self.open_context_menu)


        self.dict = {}

    def search(self, tekst):
        all_items = self.model.all_items
        counter = 0
        for i in all_items:
            if i.name == tekst:
                nadjen = i.parent
        
        for it in nadjen.elements:
            if it.name == tekst:
                self.searchi(counter, tekst)
                break
            else:
                counter +=1

    def searchi(self,counter, tekst):
        print(tekst)
        all_items = self.model.all_items
        #counter = 0
        for item in all_items:
            if item.name == tekst:
                parent = item.parent
                indeks = self.model.createIndex(counter, 0, item)
                self.setCurrentIndex(indeks)
                # ne znam da l da ostavimo da otvori u workspace za taj koji pronadje
                #self.item_clicked()
                break

    def item_clicked(self):
        index = self.selectedIndexes()
        selected_name = index[0].data()
        self.lista_contenta = []
        self.lista_stranica = []
        self.lista_slotova = []
        all_items = self.model.all_items

        for item in all_items:
            if selected_name == item.name and item.title == "Collection":
                self.collection_manager = CollectionManager(self.main_window, item.name)
                break
            if selected_name == item.name and item.title == "Document":
                self.document_manager = DocumentManager(self.main_window, item.name)
                 
                # print(item.elements, "ovo su elementi itema", item.name)
                if (len(item.elements)) >= 1:
                    self.postoji_content = True
                else:
                    self.postoji_content = False
                break
            if selected_name == item.name and item.title == "Content":
                self.content_manager = ContentManager(self.main_window, item.name)
                break
            if selected_name == item.name and item.title == "Page":
                self.page_manager = PageManager(self.main_window, item.name)
                self.lista_page.clear()
                for i in item.elements:
                    self.lista_page.append(i)
                self.page_manager.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\desktop-image.png"), 'Show',self, triggered = self.page_show))
                break
            if selected_name == item.name and item.title == "Slot":
                self.lista.clear()
                for i in item.elements:
                    self.lista.append(i)
                self.slot_manager = SlotManager(self.main_window, item.name)
                self.slot_manager.edit_menu.addAction(QtWidgets.QAction(QtGui.QIcon("resources\icons\model\desktop-image.png"), "Show", self, triggered = self.show))
                break

        for item in all_items:
            if selected_name == item.name and hasattr(item, 'file_path'):
                self.file_path = item.file_path
                self.extension = self.file_path.split(".")[-1]
                #print("ovo je", self.extension)
                self.main_window.check_extension_2(self.file_path, self.extension)
                #print("path", self.file_path)
                break
    
    def open_context_menu(self):
        self.menu.exec_(QtGui.QCursor.pos())
        
    def populate_data(self):
        self.deserialize()
        self.model = TreeModel(self.workspace, self)
        self.setModel(self.model)

        self.timer_callback()


    def timer_callback(self):
        self.serialize()
        self.thread = threading.Timer(60, self.timer_callback)
        self.thread.daemon = True
        self.thread.start()



    def serialize(self): 
        print("serializing")
        try:
            output = open(self.main_window.ulogovan_korisnik[0] + '.pkl', 'wb')    
            pickle.dump(self.workspace, output)
        except Exception: 
            print("Serialization fail.")

    def deserialize(self): 
        try:
            pkl_file = open(self.main_window.ulogovan_korisnik[0] + '.pkl', 'rb')    
            self.workspace = pickle.load(pkl_file)
        except Exception:
            print("Deserialization fail.")
            self.workspace = Workspace("Workspace")


    def delete(self):
        """
        Metoda koja je zaduzena za brisanje selektovanog.
        beginRemoveRows pocinje od selektovanog i zavrsava na selektovanom(brise se samo jedan red)
        """
        index = self.selectedIndexes()

        first = index[0].row() 

        self.delete_command.model = self.model
        self.delete_command.index = index

        self.switch.execute("Delete")

        # self.model.beginRemoveRows(index[0].parent(), first, first)
        # success = self.model.removeRow(index[0].row(), parent=index[0].parent())
        # print('Removal was a succes?:', success)
        # self.model.endRemoveRows()
        
                

    def rename(self):
        rename_dialog = RenameDialog(self)
        rename_dialog.exec_()
        if not rename_dialog.ime:
            return
        index = self.selectedIndexes()
        selected_name = index[0].data()

        first = index[0].row()

        for item in self.model.all_items: 
            if item.name == selected_name:
                self.rename_command._model = self.model
                self.rename_command._index = index
                self.rename_command._item = item
                self.rename_command._index = index     
                self.rename_command._new_name = rename_dialog.ime           
                self.switch.execute("Rename")
                break

    def insert(self):
        

        # TODO : potrebno je na neki nacin odrediti koja instanca klase ce se kreirati i dodavati
        index = self.selectedIndexes()
        selected_name = index[0].data()

        first = index[0].row()

        for item in self.model.all_items: 
            if item.name == selected_name:
                #child_title = item.child_title
                self.insert_command._model = self.model
                self.insert_command._index = index
                self.insert_command._child_title = item.child_title
                self.insert_command._item = item
                self.insert_command._index = index
                self.switch.execute("Insert")
                # self.model.beginInsertRows(index[0], 0, 0)
                # child_instance = self.model.create_child(child_title, item)
                # print(child_instance.name)
                # success = self.model.insertRow(child_instance, parent = item)
                # self.model.endInsertRows()
                break
    

        

            
        # self.slot2.setText(0, "Slot 2")
        # self.document.insertChild(0, self.slot2)
        #self.addTopLevelItem(self.document2)
    def page_show(self):
        for index in range(self.main_window.tab.count()):
            if self.main_window.tab.tabText(index) == "Graphics element":
                self.main_window.tab.removeTab(index)
                if index == 0:
                    self.main_window.remove_widget(self.main_window.tab)

        self.graphics = GraphicsElement(self.main_window)
        for i in self.lista_page:
            self.graphics.lista_pagea.append(i)

        self.main_window.add_widget(self.main_window.tab)
        self.graphics.page()
        self.main_window.remove_widget(self.main_window.widg)

    def show(self):
        for index in range(self.main_window.tab.count()):
            if self.main_window.tab.tabText(index) == "Graphics element":
                self.main_window.tab.removeTab(index)
                if index == 0:
                    self.main_window.remove_widget(self.main_window.tab)
        self.add_graphics()
 
    def add_graphics(self):
        lista = []
        self.graphics = GraphicsElement(self.main_window)
        for i in self.lista:
            lista.append(i.file_path)


        self.main_window.add_widget(self.main_window.tab)
        #self.main_window.tab.addTab("Graphic element",self.graphics)
        self.graphics.info(lista)
        self.main_window.remove_widget(self.main_window.widg)
    

        