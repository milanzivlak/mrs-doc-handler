import sys
from PySide2 import QtWidgets, QtGui, QtCore
from ui.main_window import MainWindow
from plugin_framework.plugin_registry import PluginRegistry
from ui.login import Login
import json
from PySide2.QtWidgets import QMessageBox

class ApplicationFramework(QtWidgets.QApplication):
    def __init__(self):
        super().__init__(sys.argv)

        with open('resources/users/korisnici.json', 'r') as j:
            loaded = json.load(j)
            self.korisnici = loaded

        self.logout_button = QtWidgets.QPushButton("Logout")
        self.logout_button.setIcon(QtGui.QIcon("resources/icons/user-business.png"))
        self.logout_button.clicked.connect(self.closeEvent)
        #self.logout_button.clicked.connect(self.start2())
        #self.main_window = MainWindow("Rukovalac dokumentima", QtGui.QIcon("resources/icons/document.png"))

    def closeEvent(self):
        exit = QMessageBox()
        exit.setWindowTitle("Logout")
        exit.setText("Da li ste sigurni da zelite da se odjavite?")
        exit.setStandardButtons(QMessageBox.No | QMessageBox.Yes)
        exit.setIcon(QMessageBox.Information)

        den = exit.exec_()

        if den == QMessageBox.Yes:
            #TODO: srediti logout 
            #self.quit()
            plugin_registry = PluginRegistry("plugins", self.main_window)
            for plugin in plugin_registry._plugins:
                if plugin.activated == True:
                    plugin.deactivate()
            self.main_window.ulogovan_korisnik.clear()
            self.main_window.close()
            self.login_window.ulogovan = False
            self.login()
            
        elif den == QMessageBox.No:
            return

    def login(self):
        self.login_window = Login()
        self.login_window.exec_()
        if self.login_window.ulogovan == True:
            
            with open('resources/users/korisnici.json', 'r') as j:
                loaded = json.load(j)
                self.korisnici = loaded
            self.start()
        else:
            sys.exit()


    def start(self):
        self.main_window = MainWindow("Rukovalac dokumentima", QtGui.QIcon("resources/icons/document.png"))
        self.main_window.korisnik_admin(self.login_window.ulogovan_korisnik)
        self.main_window.tree.populate_data()
        #self.main_window.ulogovan_korisnik.append(self.login_window.ulogovan_korisnik)
        #print(self.main_window.ulogovan_korisnik)
        plugin_registry = PluginRegistry("plugins", self.main_window)
        
        self.main_window.status_bar.addWidget(self.logout_button)
        self.main_window.plugin_registry = plugin_registry
        #self.main_window.ulogovan_korisnik = self.login_window.ulogovan_korisnik
        plugin_menu = QtWidgets.QMenu("Plugins")
        plugini_ulogovanog = []
        for plugin in plugin_registry._plugins:
            # mozda napraviti da se pri gasenju apk svaki plugin deaktivira
            #plugin.deactivate()
            for i in self.korisnici:
                if self.login_window.ulogovan_korisnik == i["ime"]:
                    for j in i["plugini"]:
                        if plugin.plugin_specification.name == j:
                            plugini_ulogovanog.append(j)
                            # print(plugini_ulogovanog)
            
                            plugin_row = QtWidgets.QMenu(plugin.plugin_specification.name)
                            activate = QtWidgets.QAction("Activate", self,
                                shortcut=None,
                                statusTip="Open", triggered=plugin.activate)
                            deactivate = QtWidgets.QAction("Deactivate", self,
                                shortcut=None,
                                statusTip="Open", triggered=plugin.deactivate)
                            plugin_row.addAction(activate)
                            plugin_row.addAction(deactivate)
                            plugin_menu.addMenu(plugin_row)

            #for i in plugini_ulogovanog:
            if plugin.plugin_specification.name not in plugini_ulogovanog:
                plugin_row2 = QtWidgets.QMenu(plugin.plugin_specification.name)
                ask_for_perm = QtWidgets.QAction("Zatrazi permisiju", self,
                    shortcut=None,
                    statusTip="Open", triggered=plugin.permission)
                
                plugin_row2.addAction(ask_for_perm)
                plugin_menu.addMenu(plugin_row2)
                        

            
        self.main_window.menu_bar.addMenu(plugin_menu)
        self.main_window.show()
        #sys.exit(self.exec_())

