from plugin_framework.extension import Extension
from .widget import TextEdit
import json
from PySide2 import QtWidgets

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = TextEdit(iface.central_widget)
        self.widget.hide()
        self.activated = False
        
        
        #print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        print("Text manager plugin activated!")
        self.activated = True
        self.iface.remove_widget(self.iface.widg)
        aktivacija = True
        self.iface.progres(aktivacija)
        
    def deactivate(self):
        print("Text manager plugin deactivated!")
        self.activated = False
        self.iface.remove_widget(self.widget)
        aktivacija = False
        self.iface.progres(aktivacija)
        for index in range(self.iface.tab.count()):
            if self.iface.tab.tabText(index) == "Text Manager":
                self.iface.tab.removeTab(index)
                if index == 0:
                    self.iface.remove_widget(self.iface.tab)
        #self.iface.add_widget(self.iface.widg)
    
    # Postaviti ovu metodu kao obaveznu za svaki plugin
    def in_focus(self, path): 

        if self.activated == True:
            self.widget.opened_file = path
            self.widget.open_file(path)
            self.iface.tab.addTab(self.widget, "Text Manager")
            self.iface.add_widget(self.iface.tab)
            self.iface.central_widget.setCurrentWidget(self.iface.tab)
            self.iface.tab.setCurrentIndex(self.iface.tab.count()-1)


    def permission(self):
        plugin = "Text Manager"
        self.iface.add_plugin(plugin)
        
        #self.iface.permisije(loaded["korisnici"])

    #TODO: prebaciti ovde self.widg za ispis plugina koji nisu aktivirani