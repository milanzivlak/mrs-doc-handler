from PySide2 import QtWidgets, QtGui, QtCore


class TextEdit(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        # TODO: dodati meni
        # TODO: toolbar
        #self.activated = False
        self._layout = QtWidgets.QVBoxLayout()
        self.menu_bar = QtWidgets.QMenuBar(self)
        self.text_edit = QtWidgets.QTextEdit(self)
        self.tool_bar = QtWidgets.QToolBar("Naslov", self)

        self.file_menu = QtWidgets.QMenu("File", self)
        self.help_menu = QtWidgets.QMenu("Help", self)

        # ubaceno radi testiranja akcija
        self.help_menu.addAction("Proba")
        self.file_menu.addAction("Proba")
        self.menu_bar.addMenu(self.help_menu)
        self.menu_bar.addMenu(self.file_menu)

        self._layout.addWidget(self.menu_bar)
        self._layout.addWidget(self.tool_bar)
        self._layout.addWidget(self.text_edit)
        self.setLayout(self._layout)

        self.opened_file = None


        #self.stacked_widget = parent

        #TODO: srediti ikonice
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/save.png"), "Save", self.save).setShortcut("Ctrl+S")
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/open_file.png"), "Open file", self.open_file_dialog).setShortcut("Ctrl+O")
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/undo.png"), "Undo", self.text_edit.undo)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/redo.jpg"), "Redo", self.text_edit.redo)

        # u layout dodati toolbar i menubar
        # sam widget koji je npr. textedit

    #Ctrl+o otvara dialog i odabirom fajla učitava ga i sve njegove vrednosti
    def open_file_dialog(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self,'Open File')

        if filename[0]:
            f = open(filename[0],'r')

            with f:
                data = f.read()
                self.text_edit.setText(data)

    def open_file(self, path):
        file = open(path, 'r')
        data = file.read()
        self.text_edit.setText(data)


    def save(self):
        print("Uspesno sacuvano!")
        file = open(self.opened_file, 'w')
        file.write(self.text_edit.toPlainText())
        file.close()