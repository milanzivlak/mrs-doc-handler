from plugin_framework.extension import Extension
from .widget import ImageEdit
#from ui.main_window import Login
import sys
import time
from PySide2 import QtWidgets, QtGui, QtCore

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = ImageEdit(iface.central_widget)
        self.widget.hide()
        self.activated = False

        #print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        print("Image manager plugin activated!")
        self.activated = True
        self.iface.remove_widget(self.iface.widg)
        aktivacija = True
        self.iface.progres(aktivacija)


    def deactivate(self):
        print("Test plugin deactivated!")
        self.activated = False
        self.iface.remove_widget(self.widget)
        aktivacija = False
        self.iface.progres(aktivacija)
        for index in range(self.iface.tab.count()):
            if self.iface.tab.tabText(index) == "Image Manager":
                self.iface.tab.removeTab(index)
                if index == 0:
                    self.iface.remove_widget(self.iface.tab)
        #self.iface.add_widget(self.iface.widg)

    def in_focus(self, path):
        if self.activated == True:
            thispixmap = QtGui.QPixmap(path)
            self.widget.putanja = path
            self.widget.image_edit.setPixmap(thispixmap)
            # self.iface.add_widget(self.widget)
            # self.iface.central_widget.setCurrentWidget(self.widget)
            self.iface.tab.addTab(self.widget, "Image Manager")
            self.iface.add_widget(self.iface.tab)
            self.iface.central_widget.setCurrentWidget(self.iface.tab)
            self.iface.tab.setCurrentIndex(self.iface.tab.count()-1)

    def permission(self):
        plugin = "Image manager"
        self.iface.add_plugin(plugin)