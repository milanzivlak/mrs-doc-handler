from PySide2 import QtWidgets, QtGui, QtCore
import os
import json
import operator
import pickle

class TableManager(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self._layout = QtWidgets.QVBoxLayout()
        self.new_element = {}
        self.new_element_values = []
        self.inserting_column = None
        self.matching = []
        self.insert_dict = {}
        self.number_of_atributes = 0
        self.number_of_tables = 0
        self.new_columns = []

        self.model = GenericModel(self, { "columns": []}, [])
        self.table = QtWidgets.QTableView(parent)
        self.table.setSortingEnabled(True)
        self.table.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.table.setModel(self.model)
        self.table.clicked.connect(self.row_selected)
        
        self.tool_bar = QtWidgets.QToolBar("Naslov", self)
        
        self._layout.addWidget(self.tool_bar)
        self._layout.addWidget(self.table)
        self.setLayout(self._layout)

        self.opened_file = None
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/save.png"), "Save", self.save).setShortcut("Ctrl+S")
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/open_file.png"), "Open file", self.open_file_dialog).setShortcut("Ctrl+O")
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/column-insert.png"), "Add Column", self.insert_column_form)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/insert.webp"), "Add row", self.insert_form)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/row-delete.png"), "Delete row", self.delete)

    def insert_column_form(self):
        self.dialog = QtWidgets.QDialog()
        self.button = QtWidgets.QPushButton("Dodaj")
        layout = QtWidgets.QVBoxLayout()
        self.new_column = {}


        horizontal = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel("Ime kolone")
        self.value = QtWidgets.QLineEdit()
        horizontal.addWidget(label)
        horizontal.addStretch(300)
        horizontal.addWidget(self.value)
        
        layout.addLayout(horizontal)

        layout.addWidget(self.button)
        self.dialog.setWindowTitle("Unos nove kolone")
        self.dialog.setWhatsThis("Unesite ime kolone")
        self.dialog.setLayout(layout)

        self.button.clicked.connect(self.add_column)
        self.dialog.show()

    def add_column(self):
        self.model.metadata["columns"].append(self.value.text())
        self.model = GenericModel(self, self.model.metadata, self.model.elements)
        self.table.setModel(self.model)
        self.dialog.accept()



    def insert_form(self):
        self.dialog = QtWidgets.QDialog()
        self.button = QtWidgets.QPushButton("Dodaj")
        layout = QtWidgets.QVBoxLayout()
        self.new_element = {}

        for column in self.model.metadata["columns"]:
            horizontal = QtWidgets.QHBoxLayout()
            label = QtWidgets.QLabel(column)
            self.value = QtWidgets.QLineEdit()
            self.insert_dict[column] = self.value
            horizontal.addWidget(label)
            horizontal.addStretch(300)
            horizontal.addWidget(self.value)
            
            layout.addLayout(horizontal)
            self.inserting_column = column

        layout.addWidget(self.button)
        self.dialog.setWindowTitle("Unos novog reda")
        self.dialog.setWhatsThis("Unesite vrednosti kolona")
        self.dialog.setLayout(layout)

        self.button.clicked.connect(self.insert)
        self.dialog.show()

    def insert(self):
        temp = {}
        for column in self.model.metadata['columns']:
            temp[column] = self.insert_dict[column].text()
        self.model.elements.append(temp)
        self.model = GenericModel(self, self.model.metadata, self.model.elements)
        self.table.setModel(self.model)
        self.save()
        self.dialog.accept()

    def delete(self): 
        index = self.table.selectedIndexes()
        if len(index) > 0:
            self.model.elements.pop(index[0].row())
            print(index[0].column())
            self.model = GenericModel(self, self.model.metadata, self.model.elements)
            self.table.setModel(self.model)

    def delete_tab(self, index):
        self.tab_widget.removeTab(index)

    def row_selected(self, index):
        self.selected = index.row()


    def open_file_dialog(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self,'Open File')
        if filename[0]:
            self.read(filename[0])

    def open_file(self, path):
        self.read(path)

    def read(self, path):
        self.path = path
        try:
            pkl_file = open(path, 'rb')  
            pickled_object = pickle.load(pkl_file)
            self.model = GenericModel(self, { "columns": pickled_object["metadata"]["columns"] }, pickled_object["elements"])
        except Exception as e:
            print(e)
            self.model = GenericModel(self, { "columns": []}, [])
        
        self.table.setModel(self.model)
        self.save()

    def save(self):
        try:
            pickle_object = { "metadata": self.model.metadata, "elements": self.model.elements}
            output = open(self.path, 'wb')    
            pickle.dump(pickle_object, output)
        except Exception as e: 
            print(e)

class GenericModel(QtCore.QAbstractTableModel):
    def __init__(self, parent = None, metadata = [], elements = []):
        super().__init__(parent)
        self.elements = elements
        self.metadata = metadata

    def get_element(self, index):
        return self.elements[index.row()]

    def rowCount(self, index):
        return len(self.elements)
        
    def columnCount(self, index):
        return len(self.metadata['columns'])

    def data(self, index, role = QtCore.Qt.DisplayRole):
        element = self.get_element(index)
        if role == QtCore.Qt.DisplayRole:
            ret_column = self.metadata['columns'][index.column()]
            return element[ret_column]
        return None

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.metadata['columns'][section]

        return None

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        element = self.get_element(index)
        if value == " ":
            return False

        if role == QtCore.Qt.EditRole:
            element[self.metadata['columns'][index.column()]] = value
            return True

        return False

    def flags(self, index):
        return super().flags(index) | QtCore.Qt.ItemIsEditable


    def sort(self, col, order):
        sort_by = self.metadata['columns'][col]
        self.emit(QtCore.SIGNAL("layoutAboutToBeChanged()"))
        self.elements = sorted(self.elements, key=operator.itemgetter(sort_by))        
        if order == QtCore.Qt.DescendingOrder:
            self.elements.reverse()
        self.emit(QtCore.SIGNAL("layoutChanged()"))