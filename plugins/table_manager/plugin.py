from plugin_framework.extension import Extension
from .widget import TableManager
from PySide2 import QtWidgets, QtGui, QtCore


class Plugin(Extension):
    def __init__(self, specification, iface):
        super().__init__(specification, iface)
        self.widget = TableManager(iface.central_widget)
        self.widget.hide()
        self.activated = False

    def activate(self):
        print("Table manager plugin activated!")
        self.activated = True
        self.iface.remove_widget(self.iface.widg)
        aktivacija = True
        self.iface.progres(aktivacija)
        
    def deactivate(self):
        print("Table manager plugin activated!")
        self.activated = False
        self.iface.remove_widget(self.widget)
        aktivacija = False
        self.iface.progres(aktivacija)
        for index in range(self.iface.tab.count()):
            if self.iface.tab.tabText(index) == "Table Manager":
                self.iface.tab.removeTab(index)
                if index == 0:
                    self.iface.remove_widget(self.iface.tab)

    def in_focus(self, path):
        if self.activated == True:
            self.widget.opened_file = path
            self.widget.open_file(path)
            self.iface.tab.addTab(self.widget, "Table Manager")
            self.iface.add_widget(self.iface.tab)
            self.iface.central_widget.setCurrentWidget(self.iface.tab)
            self.iface.tab.setCurrentIndex(self.iface.tab.count()-1)

    def permission(self):
        plugin = "Table Manager"
        self.iface.add_plugin(plugin)