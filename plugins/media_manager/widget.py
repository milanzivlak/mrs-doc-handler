from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtMultimedia import QMediaPlayer, QMediaPlaylist
from PySide2.QtMultimediaWidgets import QVideoWidget
import os


class MediaManager(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        # TODO: dodati meni
        # TODO: toolbar
        self._layout = QtWidgets.QVBoxLayout()
        #self.image_edit = QtWidgets.QLabel(self)
        self.media_player = QMediaPlayer()
        self.playlist = QMediaPlaylist(self.media_player)
        self.playlist.setCurrentIndex(1)
        self.media_player.setPlaylist(self.playlist)
        #self.playlist.addMedia(QtCore.QUrl("data/video_sample.mp4"))
        #self.playlist.addMedia(QtCore.QUrl("data/720.mp4"))
        #self.playlist.addMedia(QtCore.QUrl("data/audio_sample.mp3"))
        #self.playlist.setCurrentIndex(1)
        #self.media.setPlaylist(self.playlist)
        
        
        self.video_widget = QVideoWidget()
        # self.media.setVideoOutput(self.video_widget)
        # self.video_widget.show()
        
        
        # self.menu_bar = QtWidgets.QMenuBar(self)
        self.tool_bar = QtWidgets.QToolBar("Naslov", self)

        # self.file_menu = QtWidgets.QMenu("File", self)
        # self.help_menu = QtWidgets.QMenu("Help", self)

        # # ubaceno radi testiranja akcija
        # self.help_menu.addAction("Proba")
        # self.file_menu.addAction("Proba")
        # self.menu_bar.addMenu(self.help_menu)
        # self.menu_bar.addMenu(self.file_menu)

        # self._layout.addWidget(self.menu_bar)
        self._layout.addWidget(self.tool_bar)
        self._layout.addWidget(self.video_widget)
        self.setLayout(self._layout)

        self.opened_file = None



        #self.tool_bar.addAction(QtGui.QIcon("resources/icons/open_file.png"), "Open file", self.open_file_dialog).setShortcut("Ctrl+O")
       
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/play.png"), "Play", self.media_player.play)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/pause.png"), "Pause", self.media_player.pause)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/volume_up.png"), "Volume up", self.volume_up)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/volume_down.png"), "Volume down", self.volume_down)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/mute.png"), "Mute", self.mute)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/unmute.png"), "Unmute", self.unmute)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/backward.png"), "Mute", self.backward_slider)
        self.tool_bar.addAction(QtGui.QIcon("resources/icons/forward.png"), "Unmute", self.forward_slider)

        # u layout dodati toolbar i menubar
        # sam widget koji je npr. textedit

    

    def mute(self):
        self.media_player.setVolume(0)

    def unmute(self):
        self.media_player.setVolume(100)
    #Ctrl+o otvara dialog i odabirom fajla učitava ga i sve njegove vrednosti


    def forward_slider(self):
        self.media_player.setPosition(self.media_player.position() + 100*60)

    def backward_slider(self):
        self.media_player.setPosition(self.media_player.position() - 100*60)


    def volume_up(self):
        self.media_player.setVolume(self.media_player.volume() + 10)
     
    def volume_down(self):
        self.media_player.setVolume(self.media_player.volume() - 10)

    def open_file_dialog(self):
        pass
        # filename = QtWidgets.QFileDialog.getOpenFileName(self,'Open File')

        # if filename[0]:
        #     f = open(filename[0],'r')

        #     with f:
        #         data = f.read()
        #         self.text_edit.setText(data)

    def start_media(self, path):
        self.playlist.clear()
        head, tail = os.path.split(path)
        tail = "data/"+tail
        
        self.playlist.addMedia(QtCore.QUrl(str(tail)))
        #self.media.setPlaylist(self.playlist)
        self.media_player.setVideoOutput(self.video_widget)
        self.video_widget.show()
        self.media_player.play()
        #self._layout.addWidget(self.video_widget)

    # def save(self):
    #     print("Uspesno sacuvano!")
    #     file = open(self.opened_file, 'w')
    #     file.write(self.text_edit.toPlainText())
    #     file.close()