from plugin_framework.extension import Extension
from .widget import MediaManager
from PySide2 import QtWidgets, QtGui, QtCore


class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = MediaManager(iface.central_widget)
        self.widget.hide()
        self.activated = False

    def activate(self):
        print("Media manager plugin activated!")
        self.activated = True
        self.iface.remove_widget(self.iface.widg)
        aktivacija = True
        self.iface.progres(aktivacija)
        
    def deactivate(self):
        print("Media manager plugin activated!")
        self.activated = False
        self.iface.remove_widget(self.widget)
        aktivacija = False
        self.iface.progres(aktivacija)
        for index in range(self.iface.tab.count()):
            if self.iface.tab.tabText(index) == "Media Manager":
                self.iface.tab.removeTab(index)
                if index == 0:
                    self.iface.remove_widget(self.iface.tab)

    def in_focus(self, path):
        self.iface.add_widget(self.widget)
        self.widget.start_media(path)
        self.iface.tab.addTab(self.widget, "Media Manager")
        self.iface.add_widget(self.iface.tab)
        self.iface.central_widget.setCurrentWidget(self.iface.tab)
        self.iface.tab.setCurrentIndex(self.iface.tab.count()-1)
        #self.iface.central_widget.setCurrentWidget(self.widget)

    def permission(self):
        plugin = "Media Manager"
        self.iface.add_plugin(plugin)