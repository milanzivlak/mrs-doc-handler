# mrs-doc-handler

Projekat iz predmeta Metodologija razvoja softvera.

# Instalacija

- MediaManager

    - Ukoliko želite da koristite MediaManager za rad sa Audio i Video fajlovima, potrebno je da skinete i instalirate K-Lite Codec Pack.
    
     _Link: [https://www.techspot.com/downloads/4788-k-lite-codec-pack-basic.html](url)_