
class Admin:
    def __init__(self, ime, sifra, plugini=[]):
        self.ime = ime
        self.sifra = sifra
        self.plugini = plugini
    
    def get_ime(self):
        return self.ime
    
    def get_sifra(self):
        return self.sifra
    
    def get_plugini(self):
        return self.plugini